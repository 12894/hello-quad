
#include "Application.h"
#include <iostream>
#include <vector>

#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"


#include "ShaderFuncs.h"

GLfloat vertexPositions[] = {
	 0.50f, 0.50f, -0.0f, 1.0f,
	-0.50f, -0.50f, -0.0f, 1.0f,
	 0.50f, -0.50f, 0.0f, 1.0f,

	-0.50f, 0.50f, -0.0f, 1.0f,
	-0.50f, -0.50f, 0.0f, 1.0f,
	 0.50f, 0.50f, -0.0f, 1.0f,

};


GLfloat vertexColors[] = {

	1.0f, 0.0f, 0.0f, 0.5f,
	1.0f, 0.0f, 0.0f, 0.5f,
	1.0f, 0.0f, 0.0f, 0.5f,
	0.0f, 0.0f, 1.0f, 0.5f,
	0.0f, 0.0f, 1.0f, 0.5f,
	0.0f, 0.0f, 1.0f, 0.5f,
};


Application::Application() : angle(0)
							
{}

Application::~Application() 
{}

void Application::update()
{
	angle += 0.01f;

}



void Application::setup()
{
	std::string strVertexShader = loadTextFile("Shaders\\vertex.vs"); 
	std::string strFragmentShader = loadTextFile("Shaders\\Fragment.fs");
	InitializeProgram(triangle.shader, strVertexShader, strFragmentShader);
	angleID = glGetUniformLocation(triangle.shader, "angle");
	triangle.numVertex = 6;
	glGenVertexArrays(1, &triangle.vao);
	glBindVertexArray(triangle.vao);
	glGenBuffers(1, &triangle.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, triangle.vbo);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions), vertexPositions, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions)+sizeof(vertexColors), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0/*offset*/, sizeof(vertexPositions), vertexPositions);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertexPositions), sizeof(vertexColors), vertexColors);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)sizeof(vertexPositions));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	glEnable(GL_DEPTH_TEST);
}


void Application::display()
{
	//Borramos el buffer de color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Seleccionamos los shaders a usar
	glUseProgram(triangle.shader);	
	glBindVertexArray(triangle.vao);

	glUniformMatrix4fv(idTransform, 1, GL_FALSE, glm::value_ptr(transform));
	glDrawArrays(GL_TRIANGLES, 0, triangle.numVertex);
	
	/*glUniformMatrix4fv(idTransform, 1, GL_FALSE, glm::value_ptr(transform2));
	glDrawArrays(GL_TRIANGLES, 0, triangle.numVertex);*/

	//activamos el vertex array object y dibujamos

}

void Application::reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}
